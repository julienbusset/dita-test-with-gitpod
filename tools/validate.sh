#!/bin/sh
# Doc for the validation plugin at https://jason-fox.github.io/dita-ot-plugins/validate.svrl/
dita --input=src/ce-projet.ditamap --format=svrl --args.validate.mode=report
