#!/usr/bin/env python
import argparse
import os.path

SRCDIR = 'src/'

parser = argparse.ArgumentParser(description='Génère un fichier compatible avec le catalogue DITA. Préremplit le début du fichier.')
parser.add_argument('-n', '--nom', help='Nom du fichier à générer', required=True)
parser.add_argument('-t', '--type', choices=['topic','map'], help='Type DITA du fichier à générer)', required=True)
args = parser.parse_args()

match args.type:
    case 'topic':
        ext = 'dita'
    case 'map':
        ext = 'ditamap'

nomFichier = args.nom + '.' + ext

def creerFichier(nomFichier, nom, type):
    nouveauFichier = open(SRCDIR + nomFichier, 'w')
    template = open('tools/templates/' + type, 'r')
    nouveauFichier.write(template.read().replace('$filename$', nom))
    template.close()
    nouveauFichier.close()

if not os.path.isfile(SRCDIR + nomFichier):
    print('Je vais créer le fichier ' + nomFichier + ' dans le répertoire src.')
    creerFichier(nomFichier, args.nom, args.type)
    print('C’est fait !')
else:
    print('Le fichier existe déjà, je ne vais pas l’écraser. Essayez avec un autre nom de fichier.')
